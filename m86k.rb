#!/usr/bin/env ruby1.9.1
# Copyright (C) 2013 Alex Marshall "trap15" <trap15@raidenii.net>

require_relative 'parse.rb'

intxt = File.open(ARGV[0]).read.gsub(/\r\n?/, "\n")
intxt.each_line do |line|
  print m86k_rewrite_line(line) + "\n"
end

