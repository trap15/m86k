#!/usr/bin/env ruby1.9.1
# Copyright (C) 2013 Alex Marshall "trap15" <trap15@raidenii.net>

def x86_get_regs()
  return [
      "RAX",   "RCX",   "RDX",   "RBX", "RFLAGS",
       "R8",    "R9",   "R10",   "R11",
      "R12",   "R13",   "R14",   "R15",
      "RSP",   "RBP",   "RSI",   "RDI", "RIP", 
     "ZMM0",  "ZMM1",  "ZMM2",  "ZMM3",
     "ZMM4",  "ZMM5",  "ZMM6",  "ZMM7",
     "ZMM8",  "ZMM9", "ZMM10", "ZMM11",
    "ZMM12", "ZMM13", "ZMM14", "ZMM15",
    "ZMM16", "ZMM17", "ZMM18", "ZMM19",
    "ZMM20", "ZMM21", "ZMM22", "ZMM23",
    "ZMM24", "ZMM25", "ZMM26", "ZMM27",
    "ZMM28", "ZMM29", "ZMM30", "ZMM31",

      "EAX",   "ECX",   "EDX",   "EBX", "EFLAGS",
      "R8D",   "R9D",  "R10D",  "R11D",
     "R12D",  "R13D",  "R14D",  "R15D",
      "ESP",   "EBP",   "ESI",   "EDI", "EIP",
     "YMM0",  "YMM1",  "YMM2",  "YMM3",
     "YMM4",  "YMM5",  "YMM6",  "YMM7",
     "YMM8",  "YMM9", "YMM10", "YMM11",
    "YMM12", "YMM13", "YMM14", "YMM15",

       "AX",    "CX",    "DX",    "BX", "FLAGS",
      "R8W",   "R9W",  "R10W",  "R11W",
     "R12W",  "R13W",  "R14W",  "R15W",
       "SP",    "BP",    "SI",    "DI", "IP",
     "XMM0",  "XMM1",  "XMM2",  "XMM3",
     "XMM4",  "XMM5",  "XMM6",  "XMM7",
     "XMM8",  "XMM9", "XMM10", "XMM11",
    "XMM12", "XMM13", "XMM14", "XMM15",

       "AH",    "CH",    "DH",    "BH",
       "AL",    "CL",    "DL",    "BL",
      "BPL",   "SPL",   "SIL",   "DIL",
      "R8B",   "R9B",  "R10B",  "R11B",
     "R12B",  "R13B",  "R14B",  "R15B",

       "CS",    "DS",    "SS",
       "ES",    "FS",    "GS",
    ]
end

def x86_num_conv(val, sz)
  return val
end

def x86_reg_getname(reg, sz)
  case sz
    when 1
      case reg
        when  "AL",  "BL",  "CL",  "DL",
              "AH",  "BH",  "CH",  "DH",
             "SPL", "BPL", "SIL", "DIL"
          return reg
        else # R** regs
          return reg + "B"
      end
    when 2
      case reg
        when  "AL",  "BL",  "CL",  "DL",
              "AH",  "BH",  "CH",  "DH"
          return reg[0] + "X"
        when "SPL", "BPL", "SIL", "DIL"
          return reg[0,2]
        else # R** regs
          return reg + "W"
      end
    when 4
      case reg
        when  "AL",  "BL",  "CL",  "DL",
              "AH",  "BH",  "CH",  "DH"
          return "E" + reg[0] + "X"
        when "SPL", "BPL", "SIL", "DIL"
          return "E" + reg[0,2]
        else # R** regs
          return reg + "D"
      end
    when 8
      case reg
        when  "AL",  "BL",  "CL",  "DL",
              "AH",  "BH",  "CH",  "DH"
          return "R" + reg[0] + "X"
        when "SPL", "BPL", "SIL", "DIL"
          return "R" + reg[0,2]
        else # R** regs
          return reg
      end
  end
end

def x86_reg_conv(reg, sz)
  case reg
    when "RAX", "EAX", "AX", "AL"
      x86_reg_getname("AL", sz)
    when "RBX", "EBX", "BX", "BL"
      x86_reg_getname("BL", sz)
    when "RCX", "ECX", "CX", "CL"
      x86_reg_getname("CL", sz)
    when "RDX", "EDX", "DX", "DL"
      x86_reg_getname("DL", sz)
    when "R8", "R8D", "R8W", "R8B"
      x86_reg_getname("R8", sz)
    when "R9", "R9D", "R9W", "R9B"
      x86_reg_getname("R9", sz)
    when "R10", "R10D", "R10W", "R10B"
      x86_reg_getname("R10", sz)
    when "R11", "R11D", "R11W", "R11B"
      x86_reg_getname("R11", sz)
    when "R12", "R12D", "R12W", "R12B"
      x86_reg_getname("R12", sz)
    when "R13", "R13D", "R13W", "R13B"
      x86_reg_getname("R13", sz)
    when "R14", "R14D", "R14W", "R14B"
      x86_reg_getname("R14", sz)
    when "R15", "R15D", "R15W", "R15B"
      x86_reg_getname("R15", sz)
    when "RSP", "ESP", "SP", "SPL"
      x86_reg_getname("SPL", sz)
    when "RBP", "EBP", "BP", "BPL"
      x86_reg_getname("BPL", sz)
    when "RSI", "ESI", "SI", "SIL"
      x86_reg_getname("SIL", sz)
    when "RDI", "EDI", "DI", "DIL"
      x86_reg_getname("DIL", sz)

    when  "AH", "BH", "CH", "DH"
      x86_reg_getname(reg, sz)
  end
end

def x86_reg_size(reg)
  case reg
    when   "AH",   "BH",   "CH",   "DH",
           "AL",   "BL",   "CL",   "DL",
          "SPL",  "BPL",  "SIL",  "DIL",
          "R8B",  "R9B", "R10B", "R11B",
         "R12B", "R13B", "R14B", "R15B"
      return 1
    when   "AX",   "BX",   "CX",   "DX",
           "SP",   "BP",   "SI",   "DI",
          "R8W",  "R9W", "R10W", "R11W",
         "R12W", "R13W", "R14W", "R15W"
      return 2
    when  "EAX",  "EBX",  "ECX",  "EDX",
          "ESP",  "EBP",  "ESI",  "EDI",
          "R8D",  "R9D", "R10D", "R11D",
         "R12D", "R13D", "R14D", "R15D"
      return 4
    when "RAX", "RBX", "RCX", "RDX",
         "RSP", "RBP", "RSI", "RDI",
          "R8",  "R9", "R10", "R11",
         "R12", "R13", "R14", "R15"
      return 8
  end
end

def x86_convert_op(op)
  str = ""
  preline = ""
  postline = ""
  case op.size
    when 1
      szstr = "BYTE"
    when 2
      szstr = "WORD"
    when 4
      szstr = "DWORD"
    when 8
      szstr = "QWORD"
  end
  op.val = x86_num_conv(op.val, op.size)
  case op.type
    when OPTYPE_NUM
      str = op.val
    when OPTYPE_REG
      str = op.reg #x86_reg_conv(op.reg, op.size)
    when OPTYPE_DISP
      if op.val == "0"
        str = sprintf("%s PTR [%s]", szstr, op.reg)
      else
        str = sprintf("%s PTR [%s+%s]", szstr, op.reg, op.val)
      end
    when OPTYPE_PHRASE
      if op.val == "0"
        str = sprintf("%s PTR [%s+%s]", szstr, op.reg, op.reg2)
      else
        str = sprintf("%s PTR [%s+%s+%s]", szstr, op.reg, op.reg2, op.val)
      end
    when OPTYPE_POSTINC
      str = sprintf("%s PTR [%s]", szstr, op.reg)
      case x86_reg_size(op.reg)
        when 1
          szsfx = "B"
        when 2
          szsfx = "W"
        when 4
          szsfx = "L"
        when 8
          szsfx = "Q"
      end
      postline = sprintf("\tADD\t%s, %d", op.reg, op.size)
    when OPTYPE_POSTDEC
      str = sprintf("%s PTR [%s]", szstr, op.reg)
      case x86_reg_size(op.reg)
        when 1
          szsfx = "B"
        when 2
          szsfx = "W"
        when 4
          szsfx = "L"
        when 8
          szsfx = "Q"
      end
      postline = sprintf("\tSUB\t%s, %d", op.reg, op.size)
    when OPTYPE_PREINC
      str = sprintf("%s PTR [%s]", szstr, op.reg)
      case x86_reg_size(op.reg)
        when 1
          szsfx = "B"
        when 2
          szsfx = "W"
        when 4
          szsfx = "L"
        when 8
          szsfx = "Q"
      end
      preline = sprintf("\tADD\t%s, %d", op.reg, op.size)
    when OPTYPE_PREDEC
      str = sprintf("%s PTR [%s]", szstr, op.reg)
      case x86_reg_size(op.reg)
        when 1
          szsfx = "B"
        when 2
          szsfx = "W"
        when 4
          szsfx = "L"
        when 8
          szsfx = "Q"
      end
      preline = sprintf("\tSUB\t%s, %d", op.reg, op.size)
    when OPTYPE_ADDR
      str = sprintf("%s PTR [%s]", szstr, op.val)
    else
      str = op.val
  end
  if op.copied
    postline = ""
    preline = ""
  end
  return str, postline, preline
end

def x86_opdefsz_word(ops)
  for i in ops
    i.size = 2 if i.size == 0
  end
  return ops
end

def x86_opdefsz_long(ops)
  for i in ops
    i.size = 4 if i.size == 0
  end
  return ops
end

def x86_opswap_2(ops)
  return ops[1], ops[0]
end

# Needs to convert instruction name and reorder operands
def x86_convert_insn(insn, ops)
  str = ""
  postline = ""
  preline = ""
  case insn
    when "MOVE", "MOVEA", "MOVEQ"
      str = "MOV"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ADD", "ADDA", "ADDI", "ADDQ"
      str = "ADD"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ADDX"
      str = "ADC"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "AND", "ANDI"
      str = "AND"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ASL"
      str = "SAL"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ASR"
      str = "SAR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "BEQ"
      str = "JZ"
      ops = x86_opdefsz_long(ops)
    when "BNE"
      str = "JNZ"
      ops = x86_opdefsz_long(ops)
    when "BMI"
      str = "JS"
      ops = x86_opdefsz_long(ops)
    when "BPL"
      str = "JNS"
      ops = x86_opdefsz_long(ops)
    when "BVS"
      str = "JO"
      ops = x86_opdefsz_long(ops)
    when "BVC"
      str = "JNO"
      ops = x86_opdefsz_long(ops)
    when "BCS"
      str = "JC"
      ops = x86_opdefsz_long(ops)
    when "BCC"
      str = "JNC"
      ops = x86_opdefsz_long(ops)
    when "BLO"
      str = "JB"
      ops = x86_opdefsz_long(ops)
    when "BLS"
      str = "JBE"
      ops = x86_opdefsz_long(ops)
    when "BHS"
      str = "JAE"
      ops = x86_opdefsz_long(ops)
    when "BHI"
      str = "JA"
      ops = x86_opdefsz_long(ops)
    when "BLT"
      str = "JL"
      ops = x86_opdefsz_long(ops)
    when "BLE"
      str = "JLE"
      ops = x86_opdefsz_long(ops)
    when "BGT"
      str = "JG"
      ops = x86_opdefsz_long(ops)
    when "BGE"
      str = "JGE"
      ops = x86_opdefsz_long(ops)
    when "BRA", "JMP"
      str = "JMP"
      ops = x86_opdefsz_long(ops)
    when "BSR", "JSR"
      str = "CALL"
      ops = x86_opdefsz_long(ops)
    when "BCHG"
      str = "BTC"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "BCLR"
      str = "BTR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "BSET"
      str = "BTS"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "BTST"
      str = "BT"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "CLR"
      str = "MOV"
      ops.push Operand.new(OPTYPE_NUM, "0")
      ops = x86_opdefsz_word(ops)
    when "CMP", "CMPA", "CMPI", "CMPM"
      str = "CMP"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "DBRA"
      str = "LOOP"
      ops = x86_opdefsz_word(ops)
    when "DBEQ"
      str = "LOOPE"
      ops = x86_opdefsz_word(ops)
    when "DBNE"
      str = "LOOPNE"
      ops = x86_opdefsz_word(ops)
    when "DIVS"
      str = "IDIV"
      ops = x86_opdefsz_word(ops)
    when "DIVU"
      str = "DIV"
      ops = x86_opdefsz_word(ops)
    when "EOR", "EORI"
      str = "XOR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "EXG"
      str = "XCHG"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "EXT", "EXTB"
      str = "MOVSX"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "LEA"
      str = "LEA"
      ops = x86_opdefsz_long(ops)
      ops = x86_opswap_2(ops)
    when "LSL"
      str = "SHL"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "LSR"
      str = "SHR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "MULS"
      str = "IMUL"
      ops = x86_opdefsz_word(ops)
    when "MULU"
      str = "MUL"
      ops = x86_opdefsz_word(ops)
    when "NEG"
      str = "NEG"
      ops = x86_opdefsz_word(ops)
    when "NOP"
      str = "NOP"
    when "NOT"
      str = "NOT"
      ops = x86_opdefsz_word(ops)
    when "OR", "ORI"
      str = "OR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ROL"
      str = "ROL"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ROR"
      str = "ROR"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ROXL"
      str = "RLC"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "ROXR"
      str = "RRC"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "RTS"
      str = "RET"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "SEQ"
      str = "SETZ"
      ops = x86_opdefsz_word(ops)
    when "SNE"
      str = "SETNZ"
      ops = x86_opdefsz_word(ops)
    when "SMI"
      str = "SETS"
      ops = x86_opdefsz_word(ops)
    when "SPL"
      str = "SETNS"
      ops = x86_opdefsz_word(ops)
    when "SVS"
      str = "SETO"
      ops = x86_opdefsz_word(ops)
    when "SVC"
      str = "SETNO"
      ops = x86_opdefsz_word(ops)
    when "SCS"
      str = "SETC"
      ops = x86_opdefsz_word(ops)
    when "SCC"
      str = "SETNC"
      ops = x86_opdefsz_word(ops)
    when "SLO"
      str = "SETB"
      ops = x86_opdefsz_word(ops)
    when "SLS"
      str = "SETBE"
      ops = x86_opdefsz_word(ops)
    when "SHS"
      str = "SETAE"
      ops = x86_opdefsz_word(ops)
    when "SHI"
      str = "SETA"
      ops = x86_opdefsz_word(ops)
    when "SLT"
      str = "SETL"
      ops = x86_opdefsz_word(ops)
    when "SLE"
      str = "SETLE"
      ops = x86_opdefsz_word(ops)
    when "SGT"
      str = "SETG"
      ops = x86_opdefsz_word(ops)
    when "SGE"
      str = "SETGE"
      ops = x86_opdefsz_word(ops)
    when "SUB", "SUBA", "SUBI", "SUBQ"
      str = "SUB"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "SUBX"
      str = "SBC"
      ops = x86_opdefsz_word(ops)
      ops = x86_opswap_2(ops)
    when "TAS"
      str = "BTS"
      ops.push Operand.new(OPTYPE_NUM, "7")
      ops = x86_opdefsz_word(ops)
    when "TST"
      str = "CMP"
      ops.push Operand.new(OPTYPE_NUM, "0")
      ops = x86_opdefsz_word(ops)
    else
      str = "LOL"
  end
  return str, ops, postline, preline
end

