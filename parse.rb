#!/usr/bin/env ruby1.9.1
# Copyright (C) 2013 Alex Marshall "trap15" <trap15@raidenii.net>

require_relative 'x86.rb'

def m86k_get_regs()
  return x86_get_regs()
end

def m86k_convert_insn(insn, ops)
  return x86_convert_insn(insn, ops)
end

def m86k_convert_op(op)
  return x86_convert_op(op)
end

OPTYPE_NONE    = 0
OPTYPE_NUM     = 1
OPTYPE_REG     = 2
OPTYPE_DISP    = 3
OPTYPE_PHRASE  = 4
OPTYPE_ADDR    = 5
OPTYPE_POSTINC = 10
OPTYPE_POSTDEC = 11
OPTYPE_PREINC  = 12
OPTYPE_PREDEC  = 13
OPTYPE_UNK     = -1

class Operand
  attr_accessor :type, :val, :reg, :reg2, :addr, :size, :copied
  def initialize(type=OPTYPE_NONE, val="0", size=0, reg="", reg2="")
    @type = type
    @val = val
    @reg = reg
    @reg2 = reg2
    @size = size
    @copied = false
  end
end

class Array
  def swap!(a,b)
    self[a], self[b] = self[b], self[a]
    self
  end
end

def m86k_structify_operand(op, sz)
  oper = Operand.new
  oper.size = sz
  if /^#.+/.match(op)
    oper.type = OPTYPE_NUM
    /^#(?<val>.+)$/ =~ op
    oper.val = val
  elsif /^\([a-zA-Z0-9]+\)\+$/.match(op)
    oper.type = OPTYPE_POSTINC
    /^\((?<reg>[a-zA-Z0-9]+)\)\+$/ =~ op
    oper.reg = reg
  elsif /^\([a-zA-Z0-9]+\)\-$/.match(op)
    oper.type = OPTYPE_POSTDEC
    /^\((?<reg>[a-zA-Z0-9]+)\)\-$/ =~ op
    oper.reg = reg
  elsif /^\+\([a-zA-Z0-9]+\)$/.match(op)
    oper.type = OPTYPE_PREINC
    /^\+\((?<reg>[a-zA-Z0-9]+)\)$/ =~ op
    oper.reg = reg
  elsif /^\-\([a-zA-Z0-9]+\)$/.match(op)
    oper.type = OPTYPE_PREDEC
    /^\-\((?<reg>[a-zA-Z0-9]+)\)$/ =~ op
    oper.reg = reg
  elsif /^.*\([a-zA-Z0-9]+,\s*[a-zA-Z0-9]+\)$/.match(op)
    oper.type = OPTYPE_PHRASE
    /^(?<num>.*)\((?<reg>[a-zA-Z0-9]+),\s*(?<reg2>[a-zA-Z0-9]+)\)$/ =~ op
    if num == ""
      num = "0"
    end
    oper.val = num
    oper.reg = reg
    oper.reg2 = reg2
  elsif /^.*\(.+\)$/.match(op)
    oper.type = OPTYPE_DISP
    /^(?<num>.*)\((?<val>.+)\)$/ =~ op
    if num == "" and m86k_get_regs().rindex(val.upcase) == nil
      oper.type = OPTYPE_ADDR
      oper.val = val
    elsif num == ""
      oper.val = "0"
      oper.reg = val
    else
      oper.val = num
      oper.reg = val
    end
  else
    if m86k_get_regs().rindex(op.upcase) != nil
      oper.type = OPTYPE_REG
      oper.reg = op
    else
      oper.type = OPTYPE_UNK
      oper.val = op
    end
  end
  return oper
end

def m86k_copy_op(op)
  newop = op.dup
  newop.copied = true
  return newop
end

def m86k_convert(insn, ops)
  line = ""
  pline = ""

  insn, ops, postline, preline = m86k_convert_insn(insn, ops)
  if preline != ""
    line += preline + "\n"
  end
  if postline != ""
    pline += "\n" + postline
  end

  opers = Array.new
  for i in ops
    newop, postline, preline = m86k_convert_op(i)
    if preline != ""
      line += preline + "\n"
    end
    if postline != ""
      pline += "\n" + postline
    end
    opers.push newop
  end

  line += "\t" + insn + "\t"

  comma = false
  for i in opers
    if comma
      line += ", "
    end
    line += i
    comma = true
  end

  if pline != ""
    line += pline
  end

  return line
end

def m86k_rewrite_insn(insn, ops)
# Default width is word
  size = 0
  if insn[-2] == '.'
    case insn[-1]
      when 'B'
        size = 1
      when 'W'
        size = 2
      when 'L'
        size = 4
      when 'Q'
        size = 8
    end
    insn = insn[0..-3]
  end

  opers = Array.new
  for i in ops
    opers.push m86k_structify_operand(i, size)
  end

  line = m86k_convert(insn, opers)
  return line
end

def m86k_parse_line(line)
  /^(?<pfx>.*:)?/ =~ line
  /^(.*:)?\s*(?<insn>[a-zA-Z\.]+)\s+(?<ops>.+)$/ =~ line
  if ops != nil
    opers = ops.scan(/(?:\(.*?\)|[^,])+/)
    for i in opers
      i.gsub!(/ /,"")
    end
  else
    opers = nil
  end
  return pfx, insn, opers
end

def m86k_rewrite_line(line)
  pfx, insn, ops = m86k_parse_line(line)
  if pfx == nil
    pfx = ""
  end

  l = pfx
  if insn != nil
    insn.upcase!
    l += m86k_rewrite_insn(insn, ops)
  end

  return l
end


